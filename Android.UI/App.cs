﻿using Android.App;
using Android.Runtime;
using Core.Services;
using System;

namespace Android.UI
{
    [Application]
    public class App : Application
    {
        public App(IntPtr handle, JniHandleOwnership transfer) : base(handle, transfer)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();
            Initialize();
        }

        private static async void Initialize()
        {
            await CoreService.CreateTables();
        }
    }
}