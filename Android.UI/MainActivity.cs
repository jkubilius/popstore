﻿using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.UI.Fragments;
using Android.Views;
using Android.Widget;
using AndroidX;

namespace Android.UI
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity, BottomNavigationView.IOnNavigationItemSelectedListener
    {
        //TextView _textMessage;
        private Fragment fragment;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            //_textMessage = FindViewById<TextView>(Resource.Id.message);
            BottomNavigationView navigation = FindViewById<BottomNavigationView>(Resource.Id.navigation);
            navigation.SetOnNavigationItemSelectedListener(this);
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        public bool OnNavigationItemSelected(IMenuItem item)
        {
            fragment = null;
            switch (item.ItemId)
            {
                case Resource.Id.navigation_products:
                    //_textMessage.SetText(Resource.String.title_products);
                    fragment = new ProductsFragment();
                    break;
                    //return true;
                case Resource.Id.navigation_orders:
                    fragment = new OrdersFragment();
                    break;
                //_textMessage.SetText(Resource.String.title_orders);
                //return true;
                case Resource.Id.navigation_history:
                    fragment = new HistoryFragment();
                    break;
                //_textMessage.SetText(Resource.String.title_history);
                //return true;
                case Resource.Id.navigation_statistics:
                    fragment = new StatisticsFragment();
                    break;
                //_textMessage.SetText(Resource.String.title_statistics);
                //return true;
                case Resource.Id.navigation_user:
                    fragment = new UserFragment();
                    break;
                    //_textMessage.SetText(Resource.String.title_user);
                    //return true;
            }

            var transaction = FragmentManager.BeginTransaction();
            transaction.Replace(Resource.Id.main_container, fragment).Commit();

            return fragment != null ? true : false;
        }
    }
}

