﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLayer.Interfaces
{
    public interface IDataContext
    {

        string DbPath { get; }

        Task<IEnumerable<T>> GetAllAsync<T>() where T : class, new();

        Task SaveChangesAsync();

        Task BeginTransactionAsync();

        Task RollBackAsync();

    }
}
