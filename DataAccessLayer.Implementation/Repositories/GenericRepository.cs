﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataAccessLayer.Interfaces;
using DataLayer.Interfaces;

namespace DataAccessLayer.Implementation.Repositories
{
    public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : class, new()
    {
        private readonly IDataContext dataContext;
        public IQueryable<TEntity> All => dataContext.GetAll<TEntity>();

        public GenericRepository(IDataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public Task<int> Delete(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public Task<List<TEntity>> Get()
        {
            throw new NotImplementedException();
        }

        public Task<TEntity> Get(int id)
        {
            throw new NotImplementedException();
        }

        public Task<List<TEntity>> Get<TValue>(Expression<Func<TEntity, bool>> predicate = null, Expression<Func<TEntity, TValue>> orderBy = null)
        {
            throw new NotImplementedException();
        }

        public Task<TEntity> Get(Expression<Func<TEntity, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task<int> Insert(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> Update(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public Task SaveAsync()
        {
            dataContext.SaveChanges();
        }
    }
}
