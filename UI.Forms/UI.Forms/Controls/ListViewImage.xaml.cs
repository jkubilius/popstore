﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.PancakeView;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UI.Forms.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListViewImage : PancakeView
    {
        public ListViewImage()
        {
            InitializeComponent();
        }
    }
}