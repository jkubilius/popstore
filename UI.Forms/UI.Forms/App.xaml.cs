﻿using Prism;
using Prism.Ioc;
using UI.Forms.ViewModels;
using UI.Forms.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Core;
using Core.Services;
using Core.Repositories;
using Core.Entities;
using UI.Forms.Services;
using Rg.Plugins.Popup.Contracts;
using Rg.Plugins.Popup.Services;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace UI.Forms
{
    public partial class App
    {
        /*
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor.
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer)
        {
            Plugin.Iconize.Iconize
                .With(new Plugin.Iconize.Fonts.MaterialDesignIconsModule())
                .With(new Plugin.Iconize.Fonts.MaterialModule());
        }

        protected override async void OnInitialized()
        {
            InitializeComponent();

            await NavigationService
                .NavigateAsync("MainPage")
                .ConfigureAwait(true);
        }

        protected override async void OnStart()
        {
            base.OnStart();
            await CoreService.CreateTables().ConfigureAwait(false);

            AppCenter.Start("android=bd645d58-526e-4335-b292-d6c69ea3405a",
                  typeof(Analytics), typeof(Crashes));
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
            containerRegistry.RegisterForNavigation<ProductsPage, ProductsPageViewModel>();
            containerRegistry.RegisterForNavigation<OrderReviewPage, OrderReviewPageViewModel>();
            containerRegistry.RegisterForNavigation<OrdersPage, OrdersPageViewModel>();
            containerRegistry.RegisterForNavigation<OrderDetailsPage, OrderDetailsPageViewModel>();
            containerRegistry.RegisterForNavigation<OrderLineAddPage, OrderLineAddPageViewModel>();
            containerRegistry.RegisterForNavigation<HistoryPage, HistoryPageViewModel>();
            containerRegistry.RegisterForNavigation<StatisticsPage, StatisticsPageViewModel>();
            containerRegistry.RegisterForNavigation<UserPage, UserPageViewModel>();
            containerRegistry.RegisterForNavigation<ProductAddPage, ProductAddPageViewModel>();
            containerRegistry.RegisterForNavigation<ProductGroupsPage, ProductGroupsViewModel>();
            containerRegistry.RegisterForNavigation<ProductGroupAddPage, ProductGroupAddPageViewModel>();
            containerRegistry.RegisterForNavigation<OrderReviewPage, OrderReviewPageViewModel>();
            //containerRegistry.RegisterForNavigation<OrderExportFilterPopupPage, OrderExportFilterPopupPageViewModel>();

            //containerRegistry.Register<GenericRepository<Product>>();
            containerRegistry.Register<GenericService<Product>, ProductService>();
            containerRegistry.Register<IOrderService, OrderService>();
            containerRegistry.Register<Rg.Plugins.Popup.Pages.PopupPage, OrderExportFilterPopupPage>("OrderExportFilterPopupPage");
            containerRegistry.Register<IPopupService, PopupService>();
            containerRegistry.RegisterInstance(PopupNavigation.Instance);
            containerRegistry.RegisterInstance(Container);

            //containerRegistry.Register<GenericService<Order>>();
        }
    }
}
