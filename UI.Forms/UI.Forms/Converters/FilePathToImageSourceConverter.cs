﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace UI.Forms.Converters
{
    public class FilePathToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string path && !string.IsNullOrWhiteSpace(path))
            {
                return ImageSource.FromFile(path);
            }
            else
            {
                var imageSource = new FontImageSource
                {
                    Glyph = App.Current.Resources["Image"].ToString(),
                    FontFamily = "materialdesignicons.ttf#",
                    Color = Color.Accent,
                    Size = 24
                };

                return imageSource;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
