﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace UI.Forms.Converters
{
    public class BooleanToOppositeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (bool.TryParse(value?.ToString(), out bool bVal))
            { return !bVal; }

            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (bool.TryParse(value?.ToString(), out bool bVal))
            { return !bVal; }

            return true;
        }
    }
}
