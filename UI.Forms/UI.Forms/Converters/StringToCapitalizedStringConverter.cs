﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace UI.Forms.Converters
{
    public class StringToCapitalizedStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string text)
            { return text.ToUpperInvariant(); }

            return string.Empty;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string text)
            { return text.ToLowerInvariant(); }

            return string.Empty;
        }
    }
}
