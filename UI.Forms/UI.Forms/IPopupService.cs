﻿using Rg.Plugins.Popup.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace UI.Forms
{
    public interface IPopupService
    {
        IPopupNavigation PopupNavigation { get; }
        Task PushAsync(string name, bool animate = false);
        Task PopAsync(bool animate = false);
    }
}
