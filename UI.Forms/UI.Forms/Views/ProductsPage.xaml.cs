﻿using Xamarin.Forms;

namespace UI.Forms.Views
{
    public partial class ProductsPage : ContentPage
    {
        public ProductsPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await this.FadeTo(1, 10000, Easing.SinIn);
        }
    }
}
