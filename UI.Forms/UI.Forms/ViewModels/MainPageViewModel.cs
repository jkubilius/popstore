﻿using Prism.Navigation;

namespace UI.Forms.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        public MainPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "POP Store";
        }
    }
}
