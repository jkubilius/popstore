﻿using Core.Entities;
using PopStore.Common.Enumerators;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Linq;

namespace UI.Forms.ViewModels
{
    public class ProductGroupAddPageViewModel : ViewModelBase
    {
        private string name;
        private string description;
        private ProductGroup productGroup;
        private WriteMode writeMode;

        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }

        public string Description
        {
            get => description;
            set => SetProperty(ref description, value);
        }

        public DelegateCommand SaveCommand { get; set; }
        public DelegateCommand CancelCommand { get; }

        public ProductGroupAddPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            Title = "Create a new product group";
            SaveCommand = new DelegateCommand(Save);
            CancelCommand = new DelegateCommand(Cancel);
        }

        private async void Cancel()
        {
            await NavigationService.GoBackAsync().ConfigureAwait(true);
        }

        private async void Save()
        {
            ProductGroup productGroup = null;
            switch (writeMode)
            {
                case WriteMode.Insert:
                    {
                        productGroup = CreateNewProductGroup();
                        break;
                    }
                case WriteMode.Update:
                    {
                        productGroup = UpdateProductGroup();
                        break;
                    }
            }

            var navigationParameters = new NavigationParameters
            {
                { "productGroup", productGroup },
                { "writeMode", writeMode}
            };

            await NavigationService.GoBackAsync(navigationParameters).ConfigureAwait(true);
        }

        private ProductGroup CreateNewProductGroup()
        {
            return new ProductGroup
            {
                Title = Name,
                Description = Description,
                Date = DateTime.Now
            };
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters?.Any() == true)
            {
                var productGroup = parameters.GetValue<ProductGroup>("productGroup");
                if (productGroup != null)
                {
                    writeMode = WriteMode.Update;
                    SetProductGroup(productGroup);
                }
            }
        }

        private void SetProductGroup(ProductGroup productGroup)
        {
            this.productGroup = productGroup;

            Name = productGroup.Title;
            Description = productGroup.Description;
        }

        private ProductGroup UpdateProductGroup()
        {
            this.productGroup.Title = Name;
            this.productGroup.Description = Description;

            return this.productGroup;
        }
    }
}
