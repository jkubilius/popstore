﻿using Core.Entities;
using Core.Services;
using PopStore.Common.Enumerators;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace UI.Forms.ViewModels
{
    public class OrderDetailsPageViewModel : ViewModelBase
    {
        private readonly GenericService<OrderLine> orderLineService;
        private Order order;
        private ObservableCollection<OrderLine> orderLines;

        public ObservableCollection<OrderLine> OrderLines
        {
            get => orderLines;
            set => SetProperty(ref orderLines, value);
        }

        private readonly IPageDialogService dialogService;
        private readonly GenericService<Order> orderService;

        public DelegateCommand<object> DeleteCommand { get; }
        public DelegateCommand AddOrderLineCommand { get; }

        public DelegateCommand<object> ItemTappedCommand { get; }

        public OrderDetailsPageViewModel(INavigationService navigationService
                                 , IPageDialogService dialogService
                                 , GenericService<Order> orderService
                                 , GenericService<OrderLine> orderlineService) : base(navigationService)
        {
            this.dialogService = dialogService;
            this.orderService = orderService;
            this.orderLineService = orderlineService;

            DeleteCommand = new DelegateCommand<object>(Delete);
            AddOrderLineCommand = new DelegateCommand(NavigateToOrderLineAddPage);
            ItemTappedCommand = new DelegateCommand<object>(NavigateToOrderLineAddPage);
        }

        private async void NavigateToOrderLineAddPage()
        {
            _ = await NavigationService.NavigateAsync("OrderLineAddPage").ConfigureAwait(true);
        }

        private async void NavigateToOrderLineAddPage(object item)
        {
            if (item is OrderLine orderLine)
            {
                var parameters = new NavigationParameters
                {
                    { "orderLine", orderLine }
                };

                _ = await NavigationService.NavigateAsync("OrderLineAddPage", parameters).ConfigureAwait(true);
            }
        }

        private async void Delete(object item)
        {
            if (item is OrderLine orderLine)
            {
                var message = string.Format(CultureInfo.InvariantCulture, "Are you sure you want to delete {0} '{1}'?", orderLine.Product.Group.Title, orderLine.Product.Title);
                if (await dialogService.DisplayAlertAsync("Delete Order", message, "Yes", "No").ConfigureAwait(true))
                {
                    order.OrderLines = order.OrderLines.Where(l => l != orderLine).ToList();
                    await orderService.Update(order).ConfigureAwait(false);
                    SetOrder(order);
                    Reload();
                }
            }
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters?.Any() == true)
            {
                switch (parameters?.GetNavigationMode())
                {
                    case NavigationMode.New:
                        if (parameters.ContainsKey("order"))
                        {
                            var order = parameters.GetValue<Order>("order");
                            if (order != null)
                            {
                                SetOrder(order);
                                Load();
                            }
                        }
                        break;
                    case NavigationMode.Back:
                        if (parameters.ContainsKey("orderLine") && parameters.ContainsKey("writeMode"))
                        {
                            var orderLine = parameters.GetValue<OrderLine>("orderLine");
                            switch (parameters.GetValue<WriteMode>("writeMode"))
                            {
                                case WriteMode.Insert:
                                    {
                                        await InsertOrderLineAsync(orderLine).ConfigureAwait(false);
                                        break;
                                    }
                                case WriteMode.Update:
                                    {
                                        await UpdateOrderLineAsync(orderLine).ConfigureAwait(false);
                                        break;
                                    }
                            }
                        }
                        break;
                }
            }
        }

        private async Task UpdateOrderLineAsync(OrderLine orderLine)
        {
            await orderLineService.Update(orderLine).ConfigureAwait(false);
            Reload();
        }

        private async Task InsertOrderLineAsync(OrderLine orderLine)
        {
            this.order.OrderLines.Add(orderLine);
            await orderLineService.Add(orderLine).ConfigureAwait(false);
            await orderService.Update(this.order).ConfigureAwait(false);
            Reload();
        }

        private void SetOrder(Order order)
        {
            this.order = order;
            Title = string.Format(CultureInfo.InvariantCulture, "{0} {1} {2}€", order.ShortDate, order.Time, order.TotalAmount);
        }

        private void Load()
        {
            OrderLines = new ObservableCollection<OrderLine>(order.OrderLines);
        }

        private void Reload()
        {
            OrderLines = null;
            Load();
            SetOrder(this.order);
        }
    }
}
