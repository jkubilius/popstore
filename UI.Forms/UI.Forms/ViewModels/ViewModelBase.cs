﻿using Prism;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Diagnostics.CodeAnalysis;

namespace UI.Forms.ViewModels
{
    public class ViewModelBase : BindableBase, IActiveAware, INavigationAware, IDestructible
    {
        protected bool HasInitialized { get; set; }

        protected INavigationService NavigationService { get; }

        private string title;

        public event EventHandler IsActiveChanged;

        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        private bool isActive;

        public bool IsActive
        {
            get => isActive;
            set => SetProperty(ref isActive, value, RaiseIsActiveChanged);
        }

        public ViewModelBase(INavigationService navigationService)
        {
            NavigationService = navigationService;
        }

        [SuppressMessage("Design", "CA1030:Use events where appropriate", Justification = "<Pending>")]
        protected virtual void RaiseIsActiveChanged()
        {
            IsActiveChanged?.Invoke(this, EventArgs.Empty);
        }

        public virtual void OnNavigatedFrom(INavigationParameters parameters)
        {
        }

        public virtual void OnNavigatedTo(INavigationParameters parameters)
        {
            if (HasInitialized) return;

            HasInitialized = true;
        }

        public virtual void OnNavigatingTo(INavigationParameters parameters)
        {
        }

        public virtual void Destroy()
        {
        }
    }
}
