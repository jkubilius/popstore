﻿using Core.Services;
using NodaTime;
using NodaTime.Extensions;
using PopStore.Common.Events;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Rg.Plugins.Popup.Events;
using System;
using System.ComponentModel;

namespace UI.Forms.ViewModels
{
    public class OrderExportFilterPopupPageViewModel : ViewModelBase
    {
        private DateTime dateFrom;
        private DateTime dateTo;
        private readonly IPopupService popupService;
        private readonly IEventAggregator eventAggregator;
        private readonly IOrderService orderService;

        public DateTime DateFrom
        {
            get => dateFrom;
            set => SetProperty(ref dateFrom, value);
        }

        public DateTime DateTo
        {
            get => dateTo;
            set => SetProperty(ref dateTo, value);
        }

        private DateTime minDate = DateTime.MinValue;

        public DateTime MinDate
        {
            get => minDate;
            set
            {
                if (DateFrom < value)
                {
                    DateFrom = value;
                    DateTo = value;
                }

                SetProperty(ref minDate, value);
            }
        }

        private DateTime maxDate = DateTime.MaxValue;

        public DateTime MaxDate
        {
            get => maxDate;
            set
            {
                if (DateTo > value)
                {
                    DateTo = value;
                    DateFrom = value;
                }

                SetProperty(ref maxDate, value);
            }
        }

        public DelegateCommand SaveCommand { get; }
        public DelegateCommand CancelCommand { get; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>")]
        public OrderExportFilterPopupPageViewModel(INavigationService navigationService
            , IPopupService popupService
            , IEventAggregator eventAggregator
            , IOrderService orderService) : base(navigationService)
        {
            this.popupService = popupService;
            this.eventAggregator = eventAggregator;
            this.orderService = orderService;

            SaveCommand = new DelegateCommand(Save);
            CancelCommand = new DelegateCommand(Cancel);
            popupService.PopupNavigation.Pushing += OnPushing;
        }

        private async void OnPushing(object sender, PopupNavigationEventArgs e)
        {
            var interval = await orderService.GetOrderRange().ConfigureAwait(false);
            interval.Deconstruct(out var minLocalDate, out var maxLocalDate);
            MinDate = minLocalDate.ToDateTimeUnspecified();
            MaxDate = maxLocalDate.ToDateTimeUnspecified();
        }

        private async void Cancel()
        {
            await popupService.PopAsync(true).ConfigureAwait(true);
        }

        private async void Save()
        {
            var interval = new DateInterval(DateFrom.ToLocalDateTime().Date, DateTo.ToLocalDateTime().Date);
            eventAggregator.GetEvent<DateRangeSelectedEvent>().Publish(interval);
            await popupService.PopAsync(true).ConfigureAwait(true);
        }
    }
}
