﻿using Core.Entities;
using Core.Services;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using NodaTime;
using PopStore.Common.Events;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UI.Forms.Collections;
using UI.Forms.Extensions;
using Xamarin.Essentials;
using NavigationMode = Prism.Navigation.NavigationMode;

namespace UI.Forms.ViewModels
{
    public class OrdersPageViewModel : ViewModelBase
    {
        private readonly IOrderService orderService;
        private readonly GenericService<Product> productService;
        private readonly IEventAggregator eventAggregator;
        private readonly IPageDialogService dialogService;
        private readonly IPopupService popupService;
        private ObservableCollection<GroupingCollection<string, Order>> ordersByDate;

        public ObservableCollection<GroupingCollection<string, Order>> OrdersByDate
        {
            get => ordersByDate;
            set => SetProperty(ref ordersByDate, value);
        }

        private bool isBusy;

        public bool IsBusy
        {
            get => isBusy;
            set => SetProperty(ref isBusy, value);
        }

        public DelegateCommand<object> DeleteCommand { get; }
        public DelegateCommand<object> ItemTappedCommand { get; }
        public DelegateCommand ExportToExcelCommand { get; }

        public OrdersPageViewModel(INavigationService navigationService
                                 , IOrderService orderService
                                 , GenericService<Product> productService
                                 , IEventAggregator eventAggregator
                                , IPageDialogService dialogService
                                , IPopupService popupService
            )
            : base(navigationService)
        {
            Title = "Orders";

            this.orderService = orderService;
            this.productService = productService;
            this.eventAggregator = eventAggregator;
            this.dialogService = dialogService;
            this.popupService = popupService;

            DeleteCommand = new DelegateCommand<object>(DeleteOrder);
            ItemTappedCommand = new DelegateCommand<object>(NavigateToOrderDetailsPage);
            ExportToExcelCommand = new DelegateCommand(OpenOrderFilterPopupPage);

            IsActiveChanged += OnIsActiveChanged;
            this.eventAggregator?.GetEvent<DateRangeSelectedEvent>().Subscribe(OnDateRangeSelected);
            this.eventAggregator?.GetEvent<NewOrderEvent>().Subscribe(Reload, ThreadOption.UIThread);
        }

        private async void OpenOrderFilterPopupPage()
        {
            await popupService.PushAsync("OrderExportFilterPopupPage", animate: true).ConfigureAwait(true);
        }

        private async void OnDateRangeSelected(DateInterval interval)
        {
            IsBusy = true;
            IEnumerable<Order> orders = await orderService.GetByDateRangeAsync(interval).ConfigureAwait(false);
            await ExportToExcel(orders).ConfigureAwait(false);
        }

        private async Task ExportToExcel(IEnumerable<Order> orders)
        {
            string file = GenerateExcelFile();
            InsertDataIntoSheet(file, "Orders", orders);
            IsBusy = false;
            await Share.RequestAsync(new ShareFileRequest
            {
                Title = Title,
                File = new ShareFile(file)
            }).ConfigureAwait(true);
        }

        private string GenerateExcelFile()
        {
            //Fix for https://github.com/OfficeDev/Open-XML-SDK/issues/221
            Environment.SetEnvironmentVariable("MONO_URI_DOTNETRELATIVEORABSOLUTE", "true");

            var fileName = ("orders_" + DateTime.Now.ToString(CultureInfo.InvariantCulture)).ToValidFileName();
            var fn = $"{fileName}.xlsx";
            var file = Path.Combine(FileSystem.CacheDirectory, fn);
            SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Create(file, SpreadsheetDocumentType.Workbook);

            WorkbookPart workbookPart = spreadsheetDocument.AddWorkbookPart();
            workbookPart.Workbook = new Workbook();

            WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
            worksheetPart.Worksheet = new Worksheet(new SheetData());

            Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());
            Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };
            sheets.Append(sheet);

            workbookPart.Workbook.Save();

            spreadsheetDocument.Close();

            return file;
        }

        public void InsertDataIntoSheet(string fileName, string sheetName, IEnumerable<Order> orders)
        {
            //Fix for https://github.com/OfficeDev/Open-XML-SDK/issues/221
            Environment.SetEnvironmentVariable("MONO_URI_DOTNETRELATIVEORABSOLUTE", "true");

            // Open the document for editing
            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(fileName, true))
            {
                WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;

                //Set the sheet name on the first sheet
                Sheets sheets = workbookPart.Workbook.GetFirstChild<Sheets>();
                Sheet sheet = sheets.Elements<Sheet>().FirstOrDefault();

                sheet.Name = sheetName;

                WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();
                Row headerRow = sheetData.AppendChild(new Row());

                headerRow.Append(ConstructCell("OrderId", CellValues.Number));
                headerRow.Append(ConstructCell("OrderDate", CellValues.Date));
                headerRow.Append(ConstructCell("Product", CellValues.String));
                headerRow.Append(ConstructCell("ProductGroup", CellValues.String));
                headerRow.Append(ConstructCell("ProductDescription", CellValues.String));
                headerRow.Append(ConstructCell("ProductPrice", CellValues.Number));
                headerRow.Append(ConstructCell("ItemQuantity", CellValues.Number));
                headerRow.Append(ConstructCell("ItemAmount", CellValues.Number));

                foreach (var orderLine in orders.SelectMany(o => o.OrderLines).ToList())
                {
                    Row dataRow = sheetData.AppendChild(new Row());

                    dataRow.Append(ConstructCell(orderLine.OrderId.ToString(CultureInfo.InvariantCulture), CellValues.Number));
                    dataRow.Append(ConstructCell(orderLine.Order?.Date.ToLongTimeString(), CellValues.Date));
                    dataRow.Append(ConstructCell(orderLine.Product?.Title, CellValues.String));
                    dataRow.Append(ConstructCell(orderLine.Product?.Group?.Title, CellValues.String));
                    dataRow.Append(ConstructCell(orderLine.Product?.Description, CellValues.String));
                    dataRow.Append(ConstructCell(orderLine.Product?.Price.ToString(CultureInfo.InvariantCulture), CellValues.Number));
                    dataRow.Append(ConstructCell(orderLine.Quantity.ToString(CultureInfo.InvariantCulture), CellValues.Number));
                    dataRow.Append(ConstructCell(orderLine.ItemAmount.ToString(CultureInfo.InvariantCulture), CellValues.Number));
                }

                workbookPart.Workbook.Save();
            }
        }

        private Cell ConstructCell(string value, CellValues dataType)
        {
            return new Cell()
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType)
            };
        }

        private async void NavigateToOrderDetailsPage(object item)
        {
            if (item is Order order)
            {
                var navigationParameters = new NavigationParameters
                {
                    { "order", order }
                };

                _ = await NavigationService.NavigateAsync("OrderDetailsPage", navigationParameters).ConfigureAwait(true);
            }
        }

        private async void DeleteOrder(object obj)
        {
            if (obj is Order order)
            {
                var message = string.Format(CultureInfo.InvariantCulture, "Are you sure you want to delete order {0} {1} {2}€?", order.ShortDate, order.Time, order.TotalAmount);
                if (await dialogService.DisplayAlertAsync("Delete Order", message, "Yes", "No").ConfigureAwait(true))
                {
                    foreach (var orderLine in order.OrderLines)
                    {
                        if (orderLine.Product != null)
                        {
                            orderLine.Product.Stock++;
                            await productService.Update(orderLine.Product).ConfigureAwait(false);
                        }
                    }

                    await orderService.Delete(order).ConfigureAwait(false);

                    Reload(order);
                }
            }
        }

        private void OnIsActiveChanged(object sender, EventArgs e)
        {
            if (IsActive)
            {
                Reload(null);
            }
        }

        private async void Reload(Order order)
        {
            OrdersByDate = null;
            await Load().ConfigureAwait(true);
        }

        private async Task Load()
        {
            var orderGroupings = (await orderService.GetAll().ConfigureAwait(true)).GroupBy(o => o.ShortDate).Select(g => new GroupingCollection<string, Order>(g.Key, g.ToList()));
            var todaysGrouping = orderGroupings.FirstOrDefault(g => g.Key == DateTime.Now.ToShortDateString());
            todaysGrouping?.ChangeKey("Today");

            OrdersByDate = new ObservableCollection<GroupingCollection<string, Order>>(orderGroupings);
        }

        public async override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.GetNavigationMode() == NavigationMode.New)
            {
                await Load().ConfigureAwait(true);
            }

            if (parameters.GetNavigationMode() == NavigationMode.Refresh)
            {
                await Load().ConfigureAwait(true);
            }
        }
    }
}
