﻿using Core.Entities;
using Core.Services;
using PopStore.Common.Enumerators;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Threading.Tasks;

namespace UI.Forms.ViewModels
{
    public class ProductGroupsViewModel : ViewModelBase
    {
        private readonly GenericService<ProductGroup> productGroupService;
        private readonly IPageDialogService dialogService;
        private ObservableCollection<ProductGroup> productGroups;

        public bool IsLoaded { get; set; }

        public DelegateCommand AddProductGroupCommand { get; }
        public DelegateCommand<object> ItemTappedCommand { get; }
        public DelegateCommand<object> DeleteCommand { get; }

        public ObservableCollection<ProductGroup> ProductGroups
        {
            get => productGroups;
            set => SetProperty(ref productGroups, value);
        }

        public event Action Loading = delegate { };

        public ProductGroupsViewModel(INavigationService navigationService
                                    , GenericService<ProductGroup> productGroupService
            , IPageDialogService dialogService) : base(navigationService)
        {
            this.productGroupService = productGroupService;
            this.dialogService = dialogService;
            AddProductGroupCommand = new DelegateCommand(NavigateToProductGroupAddPage);
            ItemTappedCommand = new DelegateCommand<object>(NavigateToProductGroupAddPage);
            DeleteCommand = new DelegateCommand<object>(Delete);
            IsActiveChanged += OnIsActiveChanged;
        }

        private void OnIsActiveChanged(object sender, EventArgs e)
        {
            if (IsActive)
            {
                Reload();
            }
        }

        private async void Delete(object item)
        {
            if (item is ProductGroup productGroup)
            {
                var message = string.Format(CultureInfo.InvariantCulture, "Are you sure you want to delete '{0}'?", productGroup.Title);
                if (await dialogService.DisplayAlertAsync("Delete Product Group", message, "Yes", "No").ConfigureAwait(true))
                {
                    await productGroupService.Delete(productGroup).ConfigureAwait(false);
                    Reload();
                }
            }
        }

        private void Reload()
        {
            ProductGroups = null;
            Load();
        }

        private async void Load()
        {
            ProductGroups = new ObservableCollection<ProductGroup>(await productGroupService.GetAll().ConfigureAwait(true));
        }

        private async void NavigateToProductGroupAddPage()
        {
            _ = await NavigationService.NavigateAsync("ProductGroupAddPage").ConfigureAwait(true);
        }

        private async void NavigateToProductGroupAddPage(object item)
        {
            if (item is ProductGroup productGroup)
            {
                var navigationParameters = new NavigationParameters
                {
                    { "productGroup", productGroup }
                };

                _ = await NavigationService.NavigateAsync("ProductGroupAddPage", navigationParameters).ConfigureAwait(true);
            }
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (!(parameters is null))
            {
                switch (parameters.GetNavigationMode())
                {
                    case NavigationMode.New:
                        Load();
                        break;
                    case NavigationMode.Back:
                        if (parameters.ContainsKey("productGroup") && parameters.ContainsKey("writeMode"))
                        {
                            var productGroup = parameters.GetValue<ProductGroup>("productGroup");
                            switch (parameters.GetValue<WriteMode>("writeMode"))
                            {
                                case WriteMode.Insert:
                                    {
                                        await InsertProductGroupAsync(productGroup).ConfigureAwait(false);
                                        break;
                                    }
                                case WriteMode.Update:
                                    {
                                        await UpdateProductGroupAsync(productGroup).ConfigureAwait(false);
                                        break;
                                    }
                            }
                        }
                        break;
                }
            }
        }

        private async Task UpdateProductGroupAsync(ProductGroup productGroup)
        {
            await productGroupService.Update(productGroup).ConfigureAwait(false);
            Reload();
        }

        private async Task InsertProductGroupAsync(ProductGroup productGroup)
        {
            await productGroupService.Add(productGroup).ConfigureAwait(false);
            Reload();
        }
    }
}
