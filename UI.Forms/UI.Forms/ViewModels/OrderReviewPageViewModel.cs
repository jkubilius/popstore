﻿using Core.Entities;
using Core.Services;
using PopStore.Common.Events;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using UI.Forms.Collections;

namespace UI.Forms.ViewModels
{
    public class OrderReviewPageViewModel : ViewModelBase
    {
        private ObservableCollection<GroupingCollection<string, OrderLine>> orderLinesByGroup;
        private decimal totalAmount;
        private readonly GenericService<Product> productService;
        private readonly GenericService<Order> orderService;
        private readonly IEventAggregator eventAggregator;

        public decimal TotalAmount
        {
            get => totalAmount;
            set => SetProperty(ref totalAmount, value);
        }

        public ObservableCollection<GroupingCollection<string, OrderLine>> OrderLinesByGroup
        {
            get => orderLinesByGroup;
            set => SetProperty(ref orderLinesByGroup, value);
        }

        public DelegateCommand SaveCommand { get; }
        public DelegateCommand CancelCommand { get; }
        public DelegateCommand UpdateTotalAmountCommand { get; }

        public OrderReviewPageViewModel(INavigationService navigationService
                                      , GenericService<Product> productService
                                      , GenericService<Order> orderService
                                      , IEventAggregator eventAggregator) : base(navigationService)
        {
            this.productService = productService;
            this.orderService = orderService;
            this.eventAggregator = eventAggregator;

            SaveCommand = new DelegateCommand(Save);
            CancelCommand = new DelegateCommand(Cancel);
            UpdateTotalAmountCommand = new DelegateCommand(UpdateTotalAmount);

            PropertyChanged += OnPropertyChanged;
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
        }

        private async void Cancel()
        {
            await NavigationService.GoBackAsync().ConfigureAwait(true);
        }

        private async void Save()
        {
            var orderLines = OrderLinesByGroup.SelectMany(g => g);

            var order = new Order
            {
                Date = DateTime.Now,
                State = "Paid",
                OrderLines = orderLines.ToList()
            };

            await orderService.Add(order).ConfigureAwait(true);

            foreach (var orderLine in orderLines)
            {
                orderLine.Product.Stock -= orderLine.Quantity;
                await productService.Update(orderLine.Product).ConfigureAwait(true);
            }

            eventAggregator.GetEvent<NewOrderEvent>().Publish(order);

            await NavigationService.GoBackAsync().ConfigureAwait(true);
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.GetNavigationMode() == NavigationMode.New)
            {
                //await Load();
            }

            if (parameters?.Any() == true)
            {
                var products = parameters.GetValue<Collection<Product>>("selectedProducts");

                if (products != null)
                {
                    var orderLines = products.Select(p => new OrderLine { Product = p, ItemAmount = p.Price, Quantity = 1});
                    OrderLinesByGroup = new ObservableCollection<GroupingCollection<string, OrderLine>>((orderLines)
                        .GroupBy(p => p.Product?.Group.Title)
                        .Select(g => new GroupingCollection<string, OrderLine>(g.Key, g.ToList())));

                    UpdateTotalAmount();
                }
            }
        }

        public void UpdateTotalAmount()
        {
            TotalAmount = OrderLinesByGroup.SelectMany(g => g).Sum(o => o.ItemAmount * o.Quantity);
        }
    }
}
