﻿using Core.Entities;
using Core.Services;
using Plugin.Media;
using PopStore.Common.Enumerators;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using UI.Forms.Extensions;
using Xamarin.Forms;

namespace UI.Forms.ViewModels
{
    public class ProductAddPageViewModel : ViewModelBase
    {
        private Product product;
        private int groupId;
        private decimal price;
        private string name;
        private string description;
        private string imagePath;
        private WriteMode writeMode;
        private NavigationParameters navigationParameters;

        private readonly GenericService<ProductGroup> productGroupService;

        public ObservableCollection<ProductGroup> ProductGroups { get; set; } = new ObservableCollection<ProductGroup>();

        private ImageSource imageSource;

        public ImageSource ImageSource
        {
            get => imageSource;
            set => SetProperty(ref imageSource, value);
        }

        public int GroupId
        {
            get => groupId;
            set => SetProperty(ref groupId, value);
        }

        public decimal Price
        {
            get => price;
            set => SetProperty(ref price, value);
        }

        private int stock;

        public int Stock
        {
            get => stock;
            set => SetProperty(ref stock, value);
        }

        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }

        public WriteMode WriteMode
        {
            get => writeMode;
            set => SetProperty(ref writeMode, value);
        }

        public string Description
        {
            get => description;
            set => SetProperty(ref description, value);
        }

        private ProductGroup selectedGroup;

        public ProductGroup SelectedGroup
        {
            get => selectedGroup;
            set => SetProperty(ref selectedGroup, value);
        }

        public DelegateCommand SaveCommand { get; set; }
        public DelegateCommand CancelCommand { get; }
        public DelegateCommand ChooseImageCommand { get; }
        public bool IsLoaded { get; set; }

        public event Action Loading = delegate { };

        public ProductAddPageViewModel(INavigationService navigationService, GenericService<ProductGroup> productGroupService)
            : base(navigationService)
        {
            this.productGroupService = productGroupService;

            SaveCommand = new DelegateCommand(Save);
            CancelCommand = new DelegateCommand(Cancel);
            ChooseImageCommand = new DelegateCommand(ChooseImage);

            PropertyChanged += OnPropertyChanged;
            //Loading += Load;
        }

        private async void ChooseImage()
        {
            if (await CrossMedia.Current.Initialize().ConfigureAwait(true))
            {
                if (CrossMedia.Current.IsPickPhotoSupported)
                {
                    var image = await CrossMedia.Current.PickPhotoAsync(null).ConfigureAwait(true);
                    imagePath = image.Path;
                    ImageSource = ImageSource.FromStream(() =>
                    {
                        var stream = image.GetStream();
                        image.Dispose();
                        return stream;
                    });
                }
            }
        }

        private async void Cancel()
        {
            await NavigationService
                .GoBackAsync()
                .ConfigureAwait(true);
        }

        protected void OnPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (!(args is null))
            {
                switch (args.PropertyName)
                {
                    case nameof(WriteMode):
                        SwitchTitle();
                        break;
                }
            }
        }

        private void SwitchTitle()
        {
            switch (WriteMode)
            {
                case WriteMode.Insert:
                    {
                        Title = "Create a new product";
                        break;
                    }

                case WriteMode.Update:
                    {
                        Title = "Edit product";
                        break;
                    }
            }
        }

        private async Task Load()
        {
            if (!IsLoaded)
            {
                ProductGroups.AddRange(await productGroupService.GetAll().ConfigureAwait(true));

                IsLoaded = true;
            }
        }

        private async void Save()
        {
            Product product = null;
            switch (WriteMode)
            {
                case WriteMode.Insert:
                    {
                        product = CreateNewProduct();
                        break;
                    }
                case WriteMode.Update:
                    {
                        product = UpdateProduct();
                        break;
                    }
            }

            navigationParameters = new NavigationParameters
            {
                { "product", product },
                { "writeMode", WriteMode}
            };

            await NavigationService.GoBackAsync(navigationParameters).ConfigureAwait(true);
        }

        private Product CreateNewProduct()
        {
            return new Product
            {
                Title = Name,
                Group = SelectedGroup,
                Price = Price,
                Stock = Stock,
                Description = Description,
                Date = DateTime.Now,
                ImagePath = imagePath
            };
        }

        private Product UpdateProduct()
        {
            this.product.Title = Name;
            this.product.Group = SelectedGroup;
            this.product.Price = Price;
            this.product.Stock = Stock;
            this.product.Description = Description;
            this.product.ImagePath = imagePath;

            return this.product;
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters?.GetNavigationMode() == NavigationMode.New)
            {
                WriteMode = WriteMode.Insert;
                RaisePropertyChanged(nameof(WriteMode));
                await Load().ConfigureAwait(true);
            }

            if (parameters?.Any() == true)
            {
                var product = parameters.GetValue<Product>("product");
                if (product != null)
                {
                    WriteMode = WriteMode.Update;
                    SetProduct(product);
                }
            }
        }

        private void SetProduct(Product product)
        {
            this.product = product;

            Name = product.Title;
            SelectedGroup = ProductGroups.SingleOrDefault(g => g.Id == product.GroupId);
            Price = product.Price;
            Stock = product.Stock;
            Description = product.Description;
            imagePath = product.ImagePath;

            if (!string.IsNullOrWhiteSpace(imagePath))
            {
                ImageSource = ImageSource.FromFile(imagePath);
            }
        }
    }
}
