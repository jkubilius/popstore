﻿using Core.Entities;
using Core.Services;
using CsvHelper;
using Plugin.FilePicker;
using Plugin.FilePicker.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using PopStore.Common.Enumerators;
using PopStore.Common.Events;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UI.Forms.ClassMaps;
using UI.Forms.Collections;
using UI.Forms.Interfaces;

namespace UI.Forms.ViewModels
{
    public class ProductsPageViewModel : ViewModelBase
    {
        private readonly GenericService<Product> productService;
        private readonly GenericService<Order> orderService;
        private readonly IPageDialogService dialogService;
        private readonly IEventAggregator eventAggregator;
        private ObservableCollection<Product> selectedProducts;
        private ObservableCollection<GroupingCollection<string, Product>> productsByGroup;
        private bool isBusy;
        private decimal income = 0;

        public bool IsLoaded { get; set; }

        public ObservableCollection<Product> SelectedProducts
        {
            get => selectedProducts;
            set => SetProperty(ref selectedProducts, value);
        }

        public ObservableCollection<GroupingCollection<string, Product>> ProductsByGroup
        {
            get => productsByGroup;
            set => SetProperty(ref productsByGroup, value);
        }

        public bool IsBusy
        {
            get => isBusy;
            set => SetProperty(ref isBusy, value);
        }

        public decimal Income
        {
            get => income;
            set => SetProperty(ref income, value);
        }

        private bool isChecked;

        public bool IsChecked
        {
            get => isChecked;
            set => SetProperty(ref isChecked, value);
        }

        public DelegateCommand AddProductCommand { get; set; }
        public DelegateCommand AddProductGroupCommand { get; set; }
        public DelegateCommand<object> ItemTappedCommand { get; set; }
        public DelegateCommand<object> CreateSingleLineOrderCommand { get; set; }
        public DelegateCommand CreateMultiLineOrderCommand { get; }
        public DelegateCommand<object> DeleteCommand { get; }
        public DelegateCommand ImportProductsCommand { get; set; }
        public DelegateCommand<object> CheckedChangedCommand { get; }

        private readonly IEventTracker eventTracker;

        public ProductsPageViewModel(INavigationService navigationService
                                   , GenericService<Product> productService
                                   , GenericService<Order> orderService
                                   , IEventAggregator eventAggregator
            , IPageDialogService dialogService)
            : base(navigationService)
        {
            this.productService = productService;
            this.orderService = orderService;
            this.eventAggregator = eventAggregator;
            this.dialogService = dialogService;

            AddProductCommand = new DelegateCommand(NavigateToProductAddPage);
            AddProductGroupCommand = new DelegateCommand(NavigateToProductGroupsPage);
            ItemTappedCommand = new DelegateCommand<object>(NavigateToProductAddPage);
            CreateSingleLineOrderCommand = new DelegateCommand<object>(CreateSingleLineOrder);
            CreateMultiLineOrderCommand = new DelegateCommand(CreateMultiLineOrder);
            DeleteCommand = new DelegateCommand<object>(Delete);
            ImportProductsCommand = new DelegateCommand(OpenFilePicker);
            CheckedChangedCommand = new DelegateCommand<object>(ManageSelectedProduct);
            eventTracker = Xamarin.Forms.DependencyService.Get<IEventTracker>();
            IsActiveChanged += OnIsActiveChanged;
        }

        private async void CreateMultiLineOrder()
        {
            if (SelectedProducts?.Any() == true)
            {
                var parameters = new NavigationParameters
                {
                    { "selectedProducts", SelectedProducts }
                };

                _ = await NavigationService.NavigateAsync("OrderReviewPage", parameters).ConfigureAwait(true);
            }
        }

        private void ManageSelectedProduct(object item)
        {
            if (item is Product product)
            {
                if (SelectedProducts == null)
                {
                    SelectedProducts = new ObservableCollection<Product>();
                }

                if (SelectedProducts.Contains(product))
                {
                    SelectedProducts.Remove(product);
                }
                else
                {
                    SelectedProducts.Add(product);
                }
            }
        }

        private async void Delete(object item)
        {
            if (item is Product product)
            {
                var message = string.Format(CultureInfo.InvariantCulture, "Are you sure you want to delete {0} '{1}'?", product.Group.Title, product.Title);
                if (await dialogService.DisplayAlertAsync("Delete Product", message, "Yes", "No").ConfigureAwait(true))
                {
                    await productService.Delete(product).ConfigureAwait(false);
                    Reload();
                }
            }
        }

        private async void OpenFilePicker()
        {
            var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage).ConfigureAwait(true);
            if (status != PermissionStatus.Granted)
            {
                var permissions = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Storage).ConfigureAwait(true);
                status = permissions[Permission.Storage];
            }

            if (status == PermissionStatus.Granted)
            {
                FileData filedata = await CrossFilePicker.Current.PickFile().ConfigureAwait(true);
                var filePath = filedata.FilePath;

                await ReadFileData(filePath).ConfigureAwait(false);
            }
        }

        private async Task ReadFileData(string filePath)
        {
            using (var reader = new StreamReader(filePath))
            using (var csv = new CsvReader(reader))
            {
                csv.Configuration.RegisterClassMap<ProductMap>();
                var products = csv.GetRecords<Product>();

                await productService.AddRange(products.ToList()).ConfigureAwait(false);
            }

            Reload();
        }

        private void OnIsActiveChanged(object sender, EventArgs e)
        {
            if (IsActive)
            {
                Reload();
            }
        }

        private async void CreateSingleLineOrder(object obj)
        {
            if (obj is Product product)
            {
                var order = new Order
                {
                    Date = DateTime.Now,
                    State = "Paid",
                    OrderLines = new List<OrderLine>
                    {
                        new OrderLine { Product = product, Quantity = 1, ItemAmount = product.Price}
                    }
                };

                product.Stock--;

                await orderService.Add(order).ConfigureAwait(false);
                await productService.Update(product).ConfigureAwait(false);

                eventAggregator.GetEvent<NewOrderEvent>().Publish(order);

                Reload();
            }
        }

        private async void NavigateToProductGroupsPage()
        {
            _ = await NavigationService.NavigateAsync("ProductGroupsPage").ConfigureAwait(true);
        }

        private async void Load()
        {
            IsBusy = true;
            ProductsByGroup = new ObservableCollection<GroupingCollection<string, Product>>((await productService.GetAll().ConfigureAwait(true)).GroupBy(p => p.Group?.Title).Select(g => new GroupingCollection<string, Product>(g.Key, g.ToList())));
            await UpdateIncome().ConfigureAwait(true);
            eventTracker.SendEvent("productPageLoaded");
            IsBusy = false;
        }

        public void Reload()
        {
            ProductsByGroup = null;
            Load();
        }

        private async Task UpdateIncome()
        {
            Income = (await orderService.GetAll().ConfigureAwait(true))?.Sum(o => o?.OrderLines?.Sum(l => l?.ItemAmount)) ?? 0;
        }

        private async void NavigateToProductAddPage()
        {
            _ = await NavigationService.NavigateAsync("ProductAddPage").ConfigureAwait(true);
        }

        private async void NavigateToProductAddPage(object item)
        {
            if (item is Product product)
            {
                var navigationParameters = new NavigationParameters
                {
                    { "product", product }
                };

                _ = await NavigationService.NavigateAsync("ProductAddPage", navigationParameters).ConfigureAwait(true);
            }
        }

        private async Task InsertProductAsync(Product product)
        {
            await productService.Add(product).ConfigureAwait(false);
            Load();
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (!(parameters is null))
            {
                switch (parameters.GetNavigationMode())
                {
                    case NavigationMode.New:
                        {
                            Load();
                            break;
                        }
                    case NavigationMode.Back:
                        {
                            SelectedProducts = null;

                            if (parameters.ContainsKey("product") && parameters.ContainsKey("writeMode"))
                            {
                                var product = parameters.GetValue<Product>("product");
                                switch (parameters.GetValue<WriteMode>("writeMode"))
                                {
                                    case WriteMode.Insert:
                                        {
                                            await InsertProductAsync(product).ConfigureAwait(false);
                                            break;
                                        }
                                    case WriteMode.Update:
                                        {
                                            await UpdateProductAsync(product).ConfigureAwait(false);
                                            break;
                                        }
                                }
                            }
                        }
                        break;
                }
            }
        }

        private async Task UpdateProductAsync(Product product)
        {
            await productService.Update(product).ConfigureAwait(false);
            Load();
        }
    }
}
