﻿using Core.Entities;
using Core.Services;
using PopStore.Common.Enumerators;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace UI.Forms.ViewModels
{
    public class OrderLineAddPageViewModel : ViewModelBase
    {
        private readonly GenericService<Product> productService;

        private ObservableCollection<KeyValuePair<string, Product>> products;
        private KeyValuePair<string, Product> selectedProduct;
        private int quantity;
        private decimal itemAmount;
        private decimal totalAmount;
        private WriteMode writeMode;
        private OrderLine orderLine;

        public ObservableCollection<KeyValuePair<string, Product>> Products
        {
            get => products;
            set => SetProperty(ref products, value);
        }

        public KeyValuePair<string, Product> SelectedProduct
        {
            get => selectedProduct;
            set => SetProperty(ref selectedProduct, value);
        }

        public int Quantity
        {
            get => quantity;
            set => SetProperty(ref quantity, value);
        }

        public decimal ItemAmount
        {
            get => itemAmount;
            set => SetProperty(ref itemAmount, value);
        }

        public decimal TotalAmount
        {
            get => totalAmount;
            set => SetProperty(ref totalAmount, value);
        }

        public WriteMode WriteMode
        {
            get => writeMode;
            set => SetProperty(ref writeMode, value);
        }

        public DelegateCommand SaveCommand { get; }
        public DelegateCommand CancelCommand { get; }

        public OrderLineAddPageViewModel(INavigationService navigationService, GenericService<Product> productService) : base(navigationService)
        {
            this.productService = productService;

            SaveCommand = new DelegateCommand(Save);
            CancelCommand = new DelegateCommand(Cancel);

            PropertyChanged += OnPropertyChanged;
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            switch (args.PropertyName)
            {
                case nameof(SelectedProduct):
                    ItemAmount = SelectedProduct.Value.Price;
                    break;
                case nameof(ItemAmount):
                case nameof(Quantity):
                    TotalAmount = ItemAmount * Quantity;
                    break;
                case nameof(WriteMode):
                    SwitchTitle();
                    break;
            }
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters?.GetNavigationMode() == NavigationMode.New)
            {
                WriteMode = WriteMode.Insert;
                RaisePropertyChanged(nameof(WriteMode));
                await Load().ConfigureAwait(true);
            }

            if (parameters?.Any() == true)
            {
                var orderLine = parameters?.GetValue<OrderLine>("orderLine");
                if (orderLine != null)
                {
                    WriteMode = WriteMode.Update;
                    SetOrderLine(orderLine);
                }
            }
        }

        private void SetOrderLine(OrderLine orderLine)
        {
            this.orderLine = orderLine;

            SelectedProduct = products.SingleOrDefault(p => p.Value.Equals(orderLine.Product));
            Quantity = orderLine.Quantity;
            ItemAmount = orderLine.ItemAmount;
        }

        private async Task Load()
        {
            Products = new ObservableCollection<KeyValuePair<string, Product>>((await productService.GetAll().ConfigureAwait(true)).Select(p => new KeyValuePair<string, Product>($"{p.Group.Title} '{p.Title}'", p)));
        }

        private async void Cancel()
        {
            await NavigationService.GoBackAsync().ConfigureAwait(true);
        }

        private async void Save()
        {
            OrderLine orderLine = null;
            switch (WriteMode)
            {
                case WriteMode.Insert:
                    {
                        orderLine = CreateNewOrderLine();
                        break;
                    }
                case WriteMode.Update:
                    {
                        orderLine = UpdateOrderLine();
                        break;
                    }
            }

            var navigationParameters = new NavigationParameters
            {
                { "orderLine", orderLine },
                { "writeMode", WriteMode}
            };

            await NavigationService.GoBackAsync(navigationParameters).ConfigureAwait(true);
        }

        private void SwitchTitle()
        {
            switch (WriteMode)
            {
                case WriteMode.Insert:
                    {
                        Title = "Create a new order line";
                        break;
                    }

                case WriteMode.Update:
                    {
                        Title = "Edit order line";
                        break;
                    }
            }
        }

        private OrderLine UpdateOrderLine()
        {
            this.orderLine.Product = SelectedProduct.Value;
            this.orderLine.Quantity = Quantity;
            this.orderLine.ItemAmount = ItemAmount;

            return orderLine;
        }

        private OrderLine CreateNewOrderLine()
        {
            return new OrderLine
            {
                Product = SelectedProduct.Value,
                Quantity = Quantity,
                ItemAmount = ItemAmount
            };
        }
    }
}
