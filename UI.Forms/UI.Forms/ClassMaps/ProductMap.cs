﻿using Core.Entities;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using UI.Forms.ClassMaps.TypeConverters;

namespace UI.Forms.ClassMaps
{
    public class ProductMap : ClassMap<Product>
    {
        public ProductMap()
        {
            Map(m => m.Title).Name("Title");
            Map(m => m.Price).Name("Price");
            Map(m => m.Description).Name("Description");
            Map(m => m.Stock).Name("Stock");
            Map(m => m.Group).Name("Group")
                             .TypeConverter<ProductGroupConverter>();
        }
    }
}
