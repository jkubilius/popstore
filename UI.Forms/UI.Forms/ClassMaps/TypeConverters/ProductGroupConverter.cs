﻿using Core.Entities;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using System;
using System.Collections.Generic;
using System.Text;

namespace UI.Forms.ClassMaps.TypeConverters
{
    public class ProductGroupConverter : DefaultTypeConverter
    {
        public override object ConvertFromString(string text, IReaderRow row, MemberMapData memberMapData)
        {
            return new ProductGroup { Title = text};
        }

        public override string ConvertToString(object value, IWriterRow row, MemberMapData memberMapData)
        {
            return (value as ProductGroup)?.Title;
        }
    }
}
