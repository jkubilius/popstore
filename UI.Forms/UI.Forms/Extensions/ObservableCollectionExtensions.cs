﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace UI.Forms.Extensions
{
    public static class ObservableCollectionExtensions
    {
        /// <summary>
        /// Adds the elements of the specified collection to the end of the ObservableCollection(Of T).
        /// </summary>
        public static void AddRange<T>(this ObservableCollection<T> collection, IEnumerable<T> items)
        {
            if (items is null)
            {
                throw new ArgumentNullException(nameof(items));
            }

            if (collection is null)
            {
                throw new ArgumentNullException(nameof(collection));
            }

            foreach (var item in items)
            {
                collection.Add(item);
            }
        }
    }
}
