﻿using System;
using System.Globalization;
using System.IO;

namespace UI.Forms.Extensions
{
    public static class StringExtensions
    {
        public static string ToValidFileName(this string source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            foreach (var c in Path.GetInvalidFileNameChars())
            {
                source = source?.Replace(c.ToString(CultureInfo.InvariantCulture), string.Empty);
            }

            return source?.Replace(" ", string.Empty);
        }
    }
}
