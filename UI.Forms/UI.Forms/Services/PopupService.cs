﻿using Prism.Ioc;
using Rg.Plugins.Popup.Contracts;
using Rg.Plugins.Popup.Pages;
using System.Threading.Tasks;

namespace UI.Forms.Services
{
    public class PopupService : IPopupService
    {
        private readonly IContainerProvider containerProvider;

        public IPopupNavigation PopupNavigation { get; }

        public PopupService(IContainerProvider containerProvider
                          , IPopupNavigation popupNavigation)
        {
            this.containerProvider = containerProvider;
            this.PopupNavigation = popupNavigation;
        }

        public async Task PopAsync(bool animate = false)
        {
            await PopupNavigation.PopAsync(animate).ConfigureAwait(true);
        }

        public async Task PushAsync(string name, bool animate = false)
        {
            var page = containerProvider.Resolve<PopupPage>(name);

            await PopupNavigation.PushAsync(page, animate).ConfigureAwait(true);
        }
    }
}
