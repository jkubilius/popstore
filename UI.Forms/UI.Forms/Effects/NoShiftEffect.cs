﻿using Xamarin.Forms;


namespace UI.Forms.Effects
{
    public class NoShiftEffect : RoutingEffect
    {
        public NoShiftEffect() : base("PopStore.NoShiftEffect")
        {
        }
    }
}
