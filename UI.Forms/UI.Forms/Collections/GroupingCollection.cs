﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace UI.Forms.Collections
{
    public class GroupingCollection<TKey, T> : ObservableCollection<T>
    {
        public TKey Key { get; private set; }

        public GroupingCollection(TKey key, IEnumerable<T> items)
        {
            if (items is null)
            {
                throw new ArgumentNullException(nameof(items));
            }

            Key = key;
            foreach (var item in items)
            {
                Items.Add(item);
            }

            CollectionChanged += OnCollectionChanged;
        }

        public void ChangeKey(TKey newKey)
        {
            Key = newKey;
        }

        public void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
        }
    }
}
