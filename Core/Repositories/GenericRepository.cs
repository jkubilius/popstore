﻿using Core.Repositories.Interfaces;
using Core.Services;
using SQLite;
using SQLiteNetExtensionsAsync.Extensions;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Core.Repositories
{
    public class GenericRepository<T> : IRepository<T> where T : class, new()
    {
        private readonly SQLiteAsyncConnection db;

        public event Action Initializing = delegate { };

        public GenericRepository()
        {
            this.db = CoreService.Db;
            Initializing += OnInitializing;
            Initializing();
        }

        private async void OnInitializing()
        {
            await db.CreateTableAsync<T>().ConfigureAwait(false);
        }

        public AsyncTableQuery<T> AsQueryable() =>
            db.Table<T>();

        public async Task<List<T>> Get() =>
            await db.GetAllWithChildrenAsync<T>(recursive: true).ConfigureAwait(false);

        public async Task<List<T>> Get<TValue>(Expression<Func<T, bool>> predicate = null, Expression<Func<T, TValue>> orderBy = null)
        {
            var query = db.Table<T>();

            if (predicate != null)
                query = query.Where(predicate);

            if (orderBy != null)
                query = query.OrderBy<TValue>(orderBy);

            return await query.ToListAsync();
        }

        public async Task<T> Get(int id) =>
             await db.FindAsync<T>(id).ConfigureAwait(false);

        public async Task<T> Get(Expression<Func<T, bool>> predicate) =>
            await db.FindAsync<T>(predicate).ConfigureAwait(false);

        public async Task Insert(T entity) =>
             await db.InsertWithChildrenAsync(entity, recursive: true).ConfigureAwait(false);

        public async Task Update(T entity) =>
             await db.UpdateWithChildrenAsync(entity).ConfigureAwait(false);

        public async Task<int> Delete(T entity) =>
             await db.DeleteAsync(entity).ConfigureAwait(false);
    }
}
