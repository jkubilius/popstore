﻿using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace Core.Entities
{
    public class ProductGroup : IEquatable<ProductGroup>
    {
        [PrimaryKey, AutoIncrement, Column("_id_")]
        public int Id { get; set; }

        [MaxLength(128)]
        public string Title { get; set; }

        [MaxLength(256)]
        public string Description { get; set; }

        public DateTime Date { get; set; }

        [OneToMany]
        public List<Product> Products { get; set; }

        public bool Equals(ProductGroup other)
        {
            if (this is null || other is null)
            {
                return false;
            }

            if (string.IsNullOrEmpty(Title)
             || string.IsNullOrEmpty(other.Title))
            {
                return false;
            }
            else
            {
                return Title == other.Title;
            }
        }

        public override bool Equals(object obj) => Equals(obj as ProductGroup);
        public override int GetHashCode() => this?.Title.GetHashCode() ?? 0;
    }
}
