﻿using SQLite;
using SQLiteNetExtensions.Attributes;
using System;

namespace Core.Entities
{
    public class Product : IEquatable<Product>
    {
        [PrimaryKey, AutoIncrement, Column("_id_")]
        public int Id { get; set; }

        [ForeignKey(typeof(ProductGroup))]
        public int GroupId { get; set; }

        public decimal Price { get; set; }

        [MaxLength(128)]
        public string Title { get; set; }

        [MaxLength(256)]
        public string Description { get; set; }

        public int Stock { get; set; }
        public DateTime Date { get; set; }

        public string ImagePath { get; set; }

        [ManyToOne(foreignKey: nameof(GroupId), CascadeOperations = CascadeOperation.CascadeRead)]
        public ProductGroup Group { get; set; }

        public bool Equals(Product other)
        {
            if (this is null || other is null)
            {
                return false;
            }

            if (string.IsNullOrEmpty(Title)
                || string.IsNullOrEmpty(other.Title)
                || GroupId < 1
                || other.GroupId < 1)
            {
                return false;
            }
            else
            {
                return Title == other.Title && GroupId == other.GroupId;
            }
        }

        public override bool Equals(object obj) => Equals(obj as Product);

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = (int)2166136261;
                hash = (hash * 16777619) ^ Title?.GetHashCode() ?? 0;
                hash = (hash * 16777619) ^ GroupId.GetHashCode();
                return hash;
            }
        }
    }
}
