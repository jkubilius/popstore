﻿using Prism.Mvvm;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace Core.Entities
{
    public class OrderLine : BindableBase
    {
        [PrimaryKey, AutoIncrement, Column("_id_")]
        public int Id { get; set; }

        [ForeignKey(typeof(Order))]
        public int OrderId { get; set; }

        [ForeignKey(typeof(Product))]
        public int ProductId { get; set; }

        private int quantity;

        public int Quantity
        {
            get => quantity;
            set => SetProperty(ref quantity, value);
        }

        private decimal itemAmount;

        public decimal ItemAmount
        {
            get => itemAmount;
            set => SetProperty(ref itemAmount, value);
        }

        [ManyToOne(foreignKey: nameof(OrderId), CascadeOperations = CascadeOperation.All)]
        public Order Order { get; set; }

        [OneToOne(foreignKey: nameof(ProductId), CascadeOperations = CascadeOperation.CascadeRead)]
        public Product Product { get; set; }
    }
}
