﻿using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Entities
{
    public class Order
    {
        [PrimaryKey, AutoIncrement, Column("_id_")]
        public int Id { get; set; }

        public int UserId { get; set; }
        public int ClientId { get; set; }
        public DateTime Date { get; set; }

        [Ignore]
        public string ShortDate { get => Date.ToShortDateString(); }

        [Ignore]
        public string Time { get => Date.ToShortTimeString(); }

        [Ignore]
        public int OrderLineCount { get => OrderLines.Count; }

        [Ignore]
        public decimal TotalAmount { get => OrderLines.Sum(l => l.ItemAmount * l.Quantity); }

        [Ignore]
        public string Description { get => GetDescription(); }

        private string GetDescription()
        {
            var sb = new StringBuilder();
            if (OrderLines?.Count > 0)
            {
                foreach (var line in OrderLines)
                {
                    sb.Append(line.Product?.Group.Title)
                      .Append(" '")
                      .Append(line.Product?.Title)
                      .Append("' ")
                      .Append(line.Quantity)
                      .Append(" * ")
                      .Append(line.ItemAmount)
                      .Append("€ = ")
                      .Append(line.Quantity * line.ItemAmount)
                      .Append('€')
                      .Append("\n");
                }
            }

            return sb.ToString();
        }

        [MaxLength(64)]
        public string State { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<OrderLine> OrderLines { get; set; }
    }
}
