﻿using Core.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Services
{
    public class GenericService<T> where T : class, new()
    {
        private readonly GenericRepository<T> repository;

        public GenericService(GenericRepository<T> repository)
        {
            this.repository = repository;
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await repository.Get().ConfigureAwait(false);
        }

        public virtual async Task Add(T entity)
        {
            await repository.Insert(entity).ConfigureAwait(false);
        }

        public async Task AddRange(IList<T> entities)
        {
            foreach (var entity in entities)
            {
                await Add(entity).ConfigureAwait(false);
            }
        }

        public async Task Update(T entity)
        {
            await repository.Update(entity).ConfigureAwait(false);
        }

        public async Task Delete(T entity)
        {
            await repository.Delete(entity).ConfigureAwait(false);
        }
    }
}
