﻿using Core.Entities;
using Core.Repositories;
using System;
using System.Threading.Tasks;

namespace Core.Services
{
    public class ProductService : GenericService<Product>
    {
        private readonly GenericRepository<ProductGroup> groupRepository;

        public ProductService(GenericRepository<ProductGroup> groupRepository
                            , GenericRepository<Product> repository) : base(repository)
        {
            this.groupRepository = groupRepository;
        }

        public override async Task Add(Product product)
        {
            if (product.Group != null)
            {
                var productGroup = (await groupRepository.Get().ConfigureAwait(false)).Find(g => g.Equals(product.Group));
                if (productGroup is null)
                {
                    #region should be moved to ProductGroupService
                    if (product.Group.Date == default)
                    {
                        product.Group.Date = DateTime.Now;
                    }

                    await groupRepository.Insert(product.Group).ConfigureAwait(false);
                    #endregion
                }
                else
                {
                    product.Group = productGroup;
                }
            }

            if (product.Date == default)
            {
                product.Date = DateTime.Now;
            }

            await base.Add(product).ConfigureAwait(false);
        }
    }
}
