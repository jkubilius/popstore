﻿using Core.Entities;
using SQLite;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Core.Services
{
    public static class CoreService
    {
        private static readonly Lazy<SQLiteAsyncConnection> lazyDb = new Lazy<SQLiteAsyncConnection>(() => new SQLiteAsyncConnection(GetDbPath()));

        public static SQLiteAsyncConnection Db => lazyDb.Value;

        public static async Task CreateTables()
        {
            try
            {
                await Db.CreateTableAsync<Cart>().ConfigureAwait(false);
                await Db.CreateTableAsync<CartLine>().ConfigureAwait(false);
                await Db.CreateTableAsync<Order>().ConfigureAwait(false);
                await Db.CreateTableAsync<OrderLine>().ConfigureAwait(false);
                await Db.CreateTableAsync<Product>().ConfigureAwait(false);
                await Db.CreateTableAsync<ProductGroup>().ConfigureAwait(false);
            }
            catch (NullReferenceException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static string GetDbPath()
        {
            const string sqliteFilename = "MyDatabase.db3";
            string libraryPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var path = Path.Combine(libraryPath, sqliteFilename);
            //File.Delete(path);
            return path;
        }
    }
}
