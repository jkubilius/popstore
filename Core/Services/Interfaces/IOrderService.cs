﻿using Core.Entities;
using NodaTime;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Services
{
    public interface IOrderService
    {
        Task<IEnumerable<Order>> GetByDateRangeAsync(DateInterval interval);
        Task<IEnumerable<Order>> GetAll();

        Task Add(Order entity);

        Task AddRange(IList<Order> entities);

        Task Update(Order entity);

        Task Delete(Order entity);

        Task<DateInterval> GetOrderRange();
    }
}