﻿using Core.Entities;
using Core.Repositories;
using NodaTime;
using NodaTime.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Services
{
    public class OrderService : GenericService<Order>, IOrderService
    {
        public OrderService(GenericRepository<Order> repository) : base(repository)
        {
        }

        public async Task<IEnumerable<Order>> GetByDateRangeAsync(DateInterval interval)
        {
            interval.Deconstruct(out var dateFrom, out var dateTo);
            return (await GetAll()).Where(o => o.Date >= dateFrom.ToDateTimeUnspecified() && o.Date <= dateTo.ToDateTimeUnspecified());
        }

        public async Task<DateInterval> GetOrderRange()
        {
            var orders = await GetAll();
            if (orders?.Any() == true)
            {
                var dateFrom = orders.Min(o => o.Date);
                var dateTo = orders.Max(o => o.Date);

                return new DateInterval(dateFrom.ToLocalDateTime().Date, dateTo.ToLocalDateTime().Date);
            }
            else
            {
                throw new NullReferenceException("There are no orders.");
            }
        }
    }
}
