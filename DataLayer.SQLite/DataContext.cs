﻿using DataLayer.Interfaces;
using DataLayer.SQLite.Entities;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DataLayer.SQLite
{
    public class DataContext : IDataContext
    {
        private static readonly Lazy<IDataContext> _lazy = new Lazy<IDataContext>(() => new DataContext());

        public static IDataContext Instance => _lazy.Value;

        private readonly string _dbPath;
        private readonly SQLiteAsyncConnection _db;
        public string DbPath { get => _dbPath; }

        public DataContext()
        {
            _dbPath = GetDbPath();
            _db = new SQLiteAsyncConnection(_dbPath);

            CreateTables();
        }

        private async void CreateTables()
        {
            try
            {
                await _db.CreateTableAsync<Cart>();
                await _db.CreateTableAsync<CartLine>();
                await _db.CreateTableAsync<Order>();
                await _db.CreateTableAsync<OrderLine>();
                await _db.CreateTableAsync<Product>();
                await _db.CreateTableAsync<ProductGroup>();
            }
            catch (NullReferenceException e)
            {
                throw;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        private string GetDbPath()
        {
            var sqliteFilename = "MyDatabase.db3";
            string libraryPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var path = Path.Combine(libraryPath, sqliteFilename);

            return path;
        }

        public async Task<IEnumerable<T>> GetAllAsync<T>() where T : class, new()
        {
            return await _db.Table<T>().ToListAsync();
        }

        public void SaveChanges()
        {
            if (_db.)
            {
                _db.Commit();
            }
        }

        public void BeginTransaction()
        {
            if (!_db.IsInTransaction)
            {
                _db.BeginTransaction();
            }
        }

        public void RollBack()
        {
            if (_db.IsInTransaction)
            {
                _db.Rollback();
            }
        }
    }
}
