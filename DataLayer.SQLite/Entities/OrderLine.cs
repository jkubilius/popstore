﻿using SQLite;

namespace DataLayer.SQLite.Entities
{
    public class OrderLine
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int Quantity { get; set; }
    }
}
