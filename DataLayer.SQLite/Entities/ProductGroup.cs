﻿using SQLite;

namespace DataLayer.SQLite.Entities
{
    public class ProductGroup
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [MaxLength(128)]
        public string Title { get; set; }
        [MaxLength(256)]
        public string Description { get; set; }
    }
}
