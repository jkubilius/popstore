﻿using SQLite;
using System;

namespace DataLayer.SQLite.Entities
{
    public class CartLine
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int CartId { get; set; }
        public int ProductId { get; set; }
        [MaxLength(64)]
        public string State { get; set; }
        public DateTime Timestamp { get; set; }
        public int Quantity { get; set; }
    }
}
