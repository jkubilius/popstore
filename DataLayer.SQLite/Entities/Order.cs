﻿using SQLite;
using System;

namespace DataLayer.SQLite.Entities
{
    public class Order
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ClientId { get; set; }
        public DateTime Date { get; set; }
        public decimal TotalAmount { get; set; }
        [MaxLength(64)]
        public string State { get; set; }
    }
}
