﻿using SQLite;
using System;

namespace DataLayer.SQLite.Entities
{
    public class Product
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int GroupId { get; set; }
        public decimal Price { get; set; }
        [MaxLength(128)]
        public string Title { get; set; }
        [MaxLength(256)]
        public string Description { get; set; }
        public DateTime Date { get; set; }
    }
}
