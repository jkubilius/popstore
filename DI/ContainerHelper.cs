﻿using DataLayer.Interfaces;
using DataLayer.SQLite;
using System;
using Unity;

namespace DI
{
    public sealed class ContainerHelper
    {
        private static readonly Lazy<ContainerHelper> _lazy = new Lazy<ContainerHelper>(() => new ContainerHelper());

        public static ContainerHelper Instance => _lazy.Value;

        private ContainerHelper()
        {
            Container.RegisterType<IDataContext, DataContext>();
            //Container.RegisterType();
        }

        private static readonly Lazy<IUnityContainer> _container = new Lazy<IUnityContainer>(() => new UnityContainer());

        public static IUnityContainer Container { get => _container.Value; }

        //private static Lazy<IUnityContainer> _container = new Lazy<IUnityContainer>(() => 
        //{
        //    var container = new UnityContainer();
        //    RegisterTypes(container);
        //    return container;
        //});

        //public static IUnityContainer Container { get => _container.Value; }

        //private static void RegisterTypes(IUnityContainer container)
        //{
        //    //DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        //    //container.RegisterType<AnagramSolverContext>(new PerRequestLifetimeManager());
        //    //container.RegisterType<IDbWordRepository<WordEntity>, EfWordRepository>();
        //    //container.RegisterType<IRepository<WordEntity>, EfWordRepository>();
        //    //container.RegisterType<IDbCachedWordRepository<WordEntity>, EfCachedWordRepository>();
        //    //container.RegisterType<IDbUserLogRepository<UserLogEntity>, EfUserLogRepository>();
        //    //container.RegisterType<IDbUserRepository<UserEntity>, EfUserRepository>();
        //    //container.RegisterType<IUserService, UserService>();
        //    //container.RegisterType<IWordsService, WordsService>();
        //    //container.RegisterType<ICachingService, CachingService>();
        //    //container.RegisterType<IUserLogService, UserLogService>();
        //    ////container.RegisterType<IIpService, IpService>();

        //    //container.RegisterType<IAnagramSolverSettings, AnagramSolverSettings>();
        //    //container.RegisterType<IAnagramSolverService, AnagramSolverService>();

        //    //DynamicModuleUtility.RegisterModule(typeof(UnityPerRequestHttpModule));

        //}
    }
}
