﻿using NodaTime;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace PopStore.Common.Events
{
    public class DateRangeSelectedEvent : PubSubEvent<DateInterval> { }
}
