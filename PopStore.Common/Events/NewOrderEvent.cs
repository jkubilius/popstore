﻿using Core.Entities;
using Prism.Events;

namespace PopStore.Common.Events
{
    public class NewOrderEvent : PubSubEvent<Order> { }
}
