﻿namespace PopStore.Common.Enumerators
{
    public enum WriteMode
    {
        Insert,
        Update
    }
}
